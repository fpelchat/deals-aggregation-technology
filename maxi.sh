#!/bin/bash
DATE=$(date +"%Y-%m-%d_%H:%M:%S_%Z")
cd /usr/local/share/spidering_env
export PERL5LIB=.
./spider_maxi.pl >> /var/log/maxi-$$-$DATE 2>&1
