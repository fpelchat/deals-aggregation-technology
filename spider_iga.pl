#!/usr/bin/perl
#Copyright (c) 2012-2023 Frederick Pelchat

use strict;
use warnings;

use LWP::UserAgent;
use HTTP::Cookies;
use HTTP::Request;
use HTTP::Response;
use HTML::Entities; #decode html entities
use Cwd qw(getcwd); #needed for saving photos
use DBI; #generic database access
use File::Glob qw(:globally :nocase); #delete files using a pattern
use spider;
use Encode;
use Digest::file qw(digest_file_hex);

#global conf
my $type = 'spider';
my $site = 'iga';
my $base = 'https://www.iga.net';
my $db_user = 'cook';
my $db_pass = 'ijds#$(!@&*_ZjkzjsdDJ!aj_Zp;#$@';
my $db_host = 'localhost';
my $db_port = '3306';
my $db_name = 'cook';
#end of global conf

print "spidering site: $site\n";

#intialize database connection
#required for item's names and descriptions
my $dbh = DBI->connect("DBI:mysql:$db_name;host=$db_host;port=$db_port", $db_user, $db_pass, {RaiseError => 1});
#required for keywords
my $dbh_utf8 = DBI->connect("DBI:mysql:$db_name;host=$db_host;port=$db_port", $db_user, $db_pass, {RaiseError => 1});
$dbh_utf8->do('SET NAMES utf8');

#initialize lwp
my $ua = new LWP::UserAgent;
$ua->cookie_jar(HTTP::Cookies->new(
  'file' => "cookie_jar_$site.lwp",
  'autosave' => 1
));
#end of bootstrap

#XXX uncomment to use proxy
#$ua->proxy(['http', 'ftp', 'https'], 'http://PROXY:8080');

sub spider_item {
  my $url = undef;

  if(@_) {
    $url = shift;
  } else {
    die "url is mandatory\n";
  }

  $url = $base . $url;

  print "spidering: $url\n";

  #send the http request
  my $request = new HTTP::Request('GET', $url);
  &ua_randomize($ua);
  my $html = $ua->request($request);

  #needed for some reason or else its not gonna match anything
  my $content = $html->content;

  my $external_id = undef;
  my $num_of_size = undef;
  my $size = undef;
  my $size_unit = undef;
  if($content =~ m/<div[^>]+data-product="(.*?)"[^>]+>/s) {
    my $json = $1;

    if($json =~ m/'ProductId'\s*:\s*'(.*?)'/s) {
      $external_id = $1;
    }

    if($json =~ m/'Size'\s*:\s*'(.*?)'/s) {
      $size = $size_unit = $1;
      if($size =~ m/.*\(.*environ(.*)\).*/) {
        $size = $size_unit = $1;
      }
      $size =~ s/[^0-9x,\.]+//gi;
      if($size =~ m/(\d+)x(\d+)/i) {
        $num_of_size = $1;
        $size = $2;
      }
      $size_unit =~ s/[^kgmlun]+//gi;
    }
  }

  my ($d1, $m1, $y1, $d2, $m2, $y2);
  if($content =~ m/<div[^>]+island__container[^>]+>.+?rix.+?vigueur.+?(\d+)\s+([^\s]+)\s+(\d+).+?(\d+)\s+([^\s]+)\s+(\d+)/s) {
    $d1 = $1;
    $m1 = $2;
    $y1 = $3;
    $d2 = $4;
    $m2 = $5;
    $y2 = $6;
    $m1 = &str_month_to_num($m1);
    $m2 = &str_month_to_num($m2);
  }

  #detect dup asap so that we dont waste cpu time
  if(&product_discount_exists($dbh, $external_id, $site, sprintf('%04d-%02d-%02d 23:59:59', $y2, $m2, $d2))) {
    return 666;
  }

  #extract all photo urls
  my @photos = ($content =~ m/<img[^>]+data-large-url="(.+?)"/g);

  my $brand = undef;
  if($content =~ m/<span[^>]+itemprop="brand"[^>]*>(.*?)</s) {
    $brand = $1;
    $brand =~ s/^\s+//;
    $brand  =~ s/\s+$//;
    $brand  =~ s/\s+/ /;
    $brand = decode_entities($brand);
  }

  my $title = undef;
  if($content =~ m/<h1[^>]+class="[^"]*h3-like[^"]+product-detail__name[^"]*"[^>]*>(.*?)<\/h1>/s) {
    $title = $1;
    $title =~ s/\s*<span[^>]+>\s*/ /g;
    $title =~ s/\s*<\/span>\s*/ /g;
    $title =~ s/^\s+//;
    $title =~ s/\s+$//;
    $title =~ s/\s+/ /;
    $title = decode_entities($title);
  }

  my $discount = undef;
  if($content =~ m/<span[^>]+product-detail__price--sale[^>]+>(.*?)<\/span>/s) {
    $discount = $1;
    $discount =~ s/\s*<span[^>]+>\s*/ /g;
    $discount =~ s/\s*<\/span>\s*/ /g;
    $discount =~ s/,/./sg;
    $discount =~ s/[^0-9\.\-]+//sg;
    $discount = decode_entities($discount);
  }

  my $price = undef;
  if($content =~ m/<del[^>]+item-product__price[^>]+>(.*?)<\/span>/s) {
    $price = $1;
    $price =~ s/\s*<span[^>]+>\s*/ /g;
    $price =~ s/\s*<\/span>\s*/ /g;
    $price =~ s/,/./sg;
    $price =~ s/[^0-9\.\-]+//sg;
    $price = decode_entities($price);
  }

  my $made_in_quebec = 0;
  if($content =~ m/<img[^>]+(?:aliments\s+du|aliments\s+pr)[^>]+qu/i) {
    $made_in_quebec = 1;
  }

  my $cost_per = undef;
  my $cost_per_unit = undef;
  if($content =~ m/<div class="product-detail__info">(.*?)</s) {
    $cost_per = $1;
    $cost_per =~ s/\s*<span[^>]+>\s*/ /g;
    $cost_per =~ s/\s*<\/span>\s*/ /g;
    $cost_per =~ s/,/./sg;
    $cost_per =~ s/^\s+//;
    $cost_per =~ s/\s+$//;
    $cost_per =~ s/\s+/ /;
    $cost_per = decode_entities($cost_per);

    if($cost_per =~ m/100\s*[Gg]/) {
      if($cost_per =~ m/(\d+\.\d+)/) {
        $cost_per = $1;
      }
      $cost_per_unit = '100g';
    } elsif($cost_per =~ m/100\s*[Mm][Ll]/) {
      if($cost_per =~ m/(\d+\.\d+)/) {
        $cost_per = $1;
      }
      $cost_per_unit = '100ml';
    } elsif($cost_per =~ m/\s*unite/i) {
      if($cost_per =~ m/(\d+\.\d+)/) {
        $cost_per = $1;
      }
      $cost_per_unit = 'un';
    } elsif($cost_per =~ m/\s*feuille/i) {
      if($cost_per =~ m/(\d+\.\d+)/) {
        $cost_per = $1;
      }
      $cost_per_unit = 'feuille';
    } elsif($cost_per =~ m/\s*brassee/i) {
      if($cost_per =~ m/(\d+\.\d+)/) {
        $cost_per = $1;
      }
      $cost_per_unit = 'brassee';
    } else {
      printf("unsupported cost per and cost per unit: %s\n", $cost_per);
      $cost_per = $cost_per_unit = undef;
    }
  }

  my @prices = ($content =~ m/<div[^>]+grid__item[^>]+>[^<]+<span[^>]+detail__price[^>]+>(.*?)<\/span>[^<]+<del[^>]+product__price[^>]+>(.*?)<\/del>[^<]+<\/div>/sg);

  printf("%d prices detected\n", scalar @prices);

  if(@prices <= 0) {
    return 1337;
    #die 'no discount';
  }

  my @kwids = ();
  if($content =~ m/<ul[^>]+breadcrumb[^>]+>(.*?)<\/ul>/s) {
    my $sql_fmt = "INSERT INTO keyword (id, keyword, url) VALUES (NULL, %s, %s)";
    my $tree = $1;

    #print $tree . "\n";

    my @nodes = ($tree =~ m/<li[^>]*>(.*?)<\/li>/gs);
    foreach my $node (@nodes) {
      if($node =~ m/^\s*$/) {
        next;
      }

      my ($url, $keyword);

      if($node =~ m/<a[^>]+href="(.*?)"[^>]*>(.*?)<\/a>/s) {
        $url = $1;
        $keyword = $2;
      } else { #leaf node
        print "skipping leaf keyword node: $node\n";
        next;
        $url = undef;
        $keyword = $node;
      }

      #print $keyword . "\n";

      $keyword =~ s/\s*<span[^>]*>\s*/ /g;
      $keyword =~ s/\s*<\/span>\s*/ /g;
      $keyword =~ s/^\s+//;
      $keyword =~ s/\s+$//;
      $keyword =~ s/\s+/ /;
      $keyword = decode_entities($keyword);

      if($keyword =~ m/^\s*$/) {
        print "skipping empty keyword node: $keyword\n";
        next;
      }

      print $keyword . "\n";

      my $keyword_id = &keyword_exists($dbh_utf8, $keyword);

      printf("keyword id: %s\n", defined($keyword_id)? $keyword_id: 'NULL');

      if(!defined($keyword_id)) {
        my $qt_url = $dbh_utf8->quote($base . $url);
        my $qt_keyword = $dbh_utf8->quote($keyword);
        my $sql = sprintf($sql_fmt, $qt_keyword, $qt_url);
        $dbh_utf8->do($sql);
        $keyword_id = $dbh_utf8->{mysql_insertid};
      }

      push(@kwids, $keyword_id);
    }
  }

  #show them
  print join("\n", @photos);
  print "\n";

  printf("brand: %s\n", defined($brand)? $brand: 'NULL');
  printf("title: %s\n", defined($title)? $title: 'NULL');
  printf("discount: %s\n", defined($discount)? $discount: 'NULL');
  printf("price: %s\n", defined($price)? $price: 'NULL');
  printf("cost per: %s\n", defined($cost_per)? $cost_per: 'NULL');
  printf("cost per unit: %s\n", defined($cost_per_unit)? $cost_per_unit: 'NULL');
  printf("made in quebec: %s\n", $made_in_quebec? 'yes': 'no');
  printf("%04d-%02d-%02d 00:00:00 until %04d-%02d-%02d 23:59:59 inclusively\n", $y1, $m1, $d1, $y2, $m2, $d2);
  printf("%s id: %s\n", $site, defined($external_id)? $external_id: 'NULL');
  printf("num of size: %s\n", defined($num_of_size)? $num_of_size: 'NULL');
  printf("size: %s\n", defined($size)? $size: 'NULL');
  printf("size unit: %s\n", defined($size_unit)? $size_unit: 'NULL');

  my $qt_brand = $dbh->quote($brand);
  my $text = Encode::decode('ISO-8859-1', $title);
  my $qt_title = $dbh_utf8->quote($text);
  my $qt_discount = $dbh->quote($discount);
  my $qt_price = $dbh->quote($price);
  my $qt_cost_per = $dbh->quote($cost_per);
  my $qt_cost_per_unit = $dbh->quote($cost_per_unit);
  my $qt_external_id = $dbh->quote($external_id);
  my $qt_url = $dbh->quote($url);
  my $qt_num_of_size = $dbh->quote($num_of_size);
  my $qt_size = $dbh->quote($size);
  my $qt_size_unit = $dbh->quote($size_unit);

  my $cwd = getcwd();
  my $spidered_images_dir = 'spidered_images/';

  $dbh_utf8->do("
INSERT INTO product_discount (id, site_sysname, brand, title, spidered_at, num_of_size, size, size_unit, discount, price, cost_per, cost_per_unit, made_in_quebec, valid_from,
valid_until, external_id, url) VALUES (NULL, '$site', $qt_brand, $qt_title, CURRENT_TIMESTAMP, $qt_num_of_size, $qt_size, $qt_size_unit, $qt_discount, $qt_price, $qt_cost_per,
$qt_cost_per_unit, $made_in_quebec, '$y1-$m1-$d1 00:00:00', '$y2-$m2-$d2 23:59:59', $qt_external_id, $qt_url)
  ");

  my $product_discount_id = $dbh_utf8->{mysql_insertid};

  @kwids = uniq(@kwids);
  foreach my $kwid (@kwids) {
    $dbh->do("INSERT INTO product_discount_keyword (id, product_discount_id, keyword_id) VALUES (NULL, $product_discount_id, $kwid)");
  }

  &insert_photos($dbh, $ua, @photos, $spidered_images_dir, $product_discount_id);

  return 0;
}

sub spider_all {
  my $page_fmt;

  $page_fmt = 'https://www.iga.net/fr/epicerie_en_ligne/parcourir/dans-les-circulaires?page=%d&pageSize=%d&sort=IsInPromotionCurrentPeriod:desc;FlyerImpactIndCurrentPeriod:desc;score:desc';

  my $curr_page = 1;
  my $num_items_per_page = 200;

  my $count = 0;

  do {
    my $page_url = sprintf($page_fmt, $curr_page, $num_items_per_page);
    #send the http request
    my $request = new HTTP::Request('GET', $page_url);
    &ua_randomize($ua);
    my $html = $ua->request($request);

    my $content = $html->content;

    my @items = ($content =~ m/<div[^>]+class="item-product"[^>]+>.*?<a[^>]+href="(.+?)"/sg);

    print "current items:\n";
    print join("\n", @items);
    print "\n";
    my $num_items_on_this_page = scalar @items;

    my $already_spidered_on_this_page = 0;
    foreach my $i (@items) {
      if(&spider_item($i) == 666) {
        ++$already_spidered_on_this_page;
      }

      $count++;
    }

    if($num_items_on_this_page == 0 || ($num_items_on_this_page > 0 && $num_items_on_this_page == $already_spidered_on_this_page)) {
      die("we are done at page no $curr_page ($num_items_per_page item(s) per page)\n");
    }

    $curr_page++;
  } while(1);
}

my ($single) = @ARGV;
if(defined($single)) {
  &spider_item($single);
} else {
  &spider_all();
}
