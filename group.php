<?php

include('/var/www/html/conf.php');

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    printf("Error: %s\n<br/>", $e->getMessage());
    die;
}

try {
    $stmt = $conn->prepare("
SELECT COUNT(DISTINCT pd.id) AS count, GROUP_CONCAT(DISTINCT pd.id) AS pdids
FROM product_discount pd
GROUP BY pd.site_sysname, pd.brand, pd.num_of_size, pd.size, pd.size_unit, pd.discount
ORDER BY count DESC
    ");

    $stmt->execute();
} catch(PDOException $e) {
    printf("Error: %s\n<br/>", $e->getMessage());
    die;
}

try {
    $stmt2 = $conn->prepare("TRUNCATE `group`");
    $stmt2->execute();
    $stmt2 = $conn->prepare("TRUNCATE product_discount_group");
    $stmt2->execute();
} catch(PDOException $e) {
    printf("Error: %s\n<br/>", $e->getMessage());
    die;
}

while(($grp = $stmt->fetch(PDO::FETCH_OBJ))) {
    try {
        $newgrp = $conn->prepare("INSERT INTO `group` VALUES (NULL)");
        $newgrp->execute();
        $grpid = $conn->lastInsertId();
    } catch(PDOException $e) {
        printf("Error: %s\n<br/>", $e->getMessage());
        die;
    }

    printf("group id: %d, product discount ids: %s\n", $grpid, $grp->pdids);
    $pdids = explode(',', $grp->pdids);
    foreach($pdids as $pdid) {
        try {
            $apdgrpassoc = $conn->prepare("INSERT INTO product_discount_group (id, product_discount_id, group_id) VALUES (NULL, ?, ?)");
            $apdgrpassoc->execute([$pdid, $grpid]);
        } catch(PDOException $e) {
            printf("Error: %s\n<br/>", $e->getMessage());
            die;
        }
    }
}

/*

try {
    $stmt3 = $conn->prepare("
SELECT group_id,
COUNT(DISTINCT product_discount_id) AS `count`,
GROUP_CONCAT(DISTINCT product_discount_id ORDER BY product_discount_id DESC) AS pdids
FROM product_discount_group pdg
GROUP BY group_id
HAVING COUNT(DISTINCT product_discount_id) > 0
ORDER BY `count` DESC
    ");

    $stmt3->execute();
} catch(PDOException $e) {
    printf("Error: %s\n<br/>", $e->getMessage());
    die;
}

$i = 0;

while(($grp = $stmt3->fetch(PDO::FETCH_OBJ))) {
    $pdids = explode(',', $grp->pdids);

    $group[$i] = ['group_id' => $grp->group_id, 'merged' => false, 'product_discount_ids' => []];

    foreach($pdids as $pdid) {
        try {
            $kwsforpd = $conn->prepare("
                SELECT
                    GROUP_CONCAT(DISTINCT kw.id ORDER BY kw.id ASC) AS keyword_ids,
                    LOWER(pd.brand) AS brand,
                    replace(lower(brand), ' ', 'aaaaa') as brand2,
                    pd.num_of_size,
                    pd.size,
                    pd.size_unit,
                    pd.discount,
                    pd.site_sysname,
                    pd.title
                FROM product_discount_keyword pdk
                JOIN keyword kw ON kw.id = pdk.keyword_id
                JOIN product_discount pd ON pdk.product_discount_id = pd.id
                WHERE pdk.product_discount_id = :product_discount_id
                GROUP BY pdk.product_discount_id
            ");

            $kwsforpd->bindParam(':product_discount_id', $pdid, PDO::PARAM_INT);
            $kwsforpd->execute();
            $kws = $kwsforpd->fetch(PDO::FETCH_OBJ);

            if(!empty($kws)) {
                $group[$i]['product_discount_ids'][intval($pdid)] = [
                    'product_discount_id' => intval($pdid),
                    'brand' => $kws->brand2,
                    'num_of_size' => $kws->num_of_size,
                    'size' => $kws->size,
                    'size_unit' => $kws->size_unit,
                    'discount' => $kws->discount,
                    'site' => $kws->site_sysname,
                    'title' => $kws->title,
                    'keyword_ids' => explode(',', $kws->keyword_ids)
                ];
            }
        } catch(PDOException $e) {
            printf("Error: %s\n<br/>", $e->getMessage());
            die;
        }
    }

    ++$i;
}

//print_r($group);

for($i = 0; $i < count($group); $i++) {
    if($group[$i]['merged']) {
        continue;
    }

    for($j = 0; $j < count($group); $j++) {
        if($i == $j || $group[$j]['merged']) {
            continue;
        }

        foreach($group[$i]['product_discount_ids'] as $id1k => $id1v) {

            $jmatches = 0;
            foreach($group[$j]['product_discount_ids'] as $id2k => $id2v) {
                $exit_loop = false;

                foreach($id1v['keyword_ids'] as $kwid) {
                    if(!in_array($kwid, $id2v['keyword_ids'])) {
                        $exit_loop = true;
                        break;
                    }
                }

                if($exit_loop) {
                    continue;
                }

                if($id1v['discount'] != $id2v['discount']) {
                    continue;
                }

                if($id1v['site'] != $id2v['site']) {
                    continue;
                }

                //print mb_regex_encoding();
                //mb_regex_encoding('UTF-8');

                $parts1 = explode('aaaaa', utf8_encode($id1v['brand']));
                $parts2 = explode('aaaaa', utf8_encode($id2v['brand']));

                //print_r($parts1);
                //print_r($parts2);

                $stopwords = ['les'];
                $count_words = 0;

                foreach($parts1 as $p1) {
                    if(in_array($p1, $stopwords)) {
                        continue;
                    }

                    if(in_array($p1, $parts2)) {
                        ++$count_words;
                    }
                }

                if($count_words <= 0) {
                    continue;
                }

                //we got a match merge group[j] into group[i]
                printf("we got a match merging group[%d] (%s) into group[%d] (%s)\n",
                    $j, implode(',', array_keys($group[$j]['product_discount_ids'])),
                    $i, implode(',', array_keys($group[$i]['product_discount_ids']))
                );
                //print_r($group[$j]);
                //print_r($group[$i]);
                //printf("----------------------------\n");
                $group[$j]['merged'] = true;
                ++$jmatches;
                if($jmatches > 0) {
                    foreach($group[$j]['product_discount_ids'] as $dak => $dav) {
                        $mergestmt = $conn->prepare("insert into product_discount_group
                            (id, product_discount_id, group_id)
                            values (NULL, :product_discount_id, :group_id);
                        ");

                        $mergestmt->bindParam(':product_discount_id', $dak, PDO::PARAM_INT);
                        $mergestmt->bindParam(':group_id', $group[$i]['group_id'], PDO::PARAM_INT);
                        $mergestmt->execute();
                    }

                    $delstmt = $conn->prepare("delete from product_discount_group
                        where group_id = :group_id
                    ");

                    $delstmt->bindParam(':group_id', $group[$j]['group_id'], PDO::PARAM_INT);
                    $delstmt->execute();

                    break;
                }
            }

            if($jmatches > 0) {
                break;
            }
        }
    }
}

?>
