package spider;

use strict;
use warnings FATAL => 'all';
use Exporter;
use Time::HiRes qw(gettimeofday); #unique file names
use Digest::file qw(digest_file_hex);
use Term::ANSIColor;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

$VERSION     = 1.00;
@ISA         = qw(Exporter);
@EXPORT      = qw(uniq str_month_to_num product_discount_exists keyword_exists photo_exists db_seed db_reset microtime ua_randomize spider_json_flyer_list trim_json_value insert_photos);
@EXPORT_OK   = ();
%EXPORT_TAGS = ();

sub uniq {
  my %seen;
  grep !$seen{$_}++, @_;
}

sub trim_json_value {
  $_[0] =~ s/^\s+//;
  $_[0] =~ s/\s+$//;
  $_[0] =~ s/^['"]{1}//;
  $_[0] =~ s/['"]{1}$//;
  $_[0] =~ s/\s{2,}/ /g;
}

sub str_month_to_num {
  my $m1 = shift;

  if($m1 =~ m/jan/i) {
    $m1 = 1;
  } elsif($m1 =~ m/vrier/i) {
    $m1 = 2;
  } elsif($m1 =~ m/mar/i) {
    $m1 = 3;
  } elsif($m1 =~ m/avr/i) {
    $m1 = 4;
  } elsif($m1 =~ m/mai/i) {
    $m1 = 5;
  } elsif($m1 =~ m/juin/i) {
    $m1 = 6;
  } elsif($m1 =~ m/juillet/i) {
    $m1 = 7;
  } elsif($m1 =~ m/ao/i) {
    $m1 = 8;
  } elsif($m1 =~ m/sep/i) {
    $m1 = 9;
  } elsif($m1 =~ m/oct/i) {
    $m1 = 10;
  } elsif($m1 =~ m/nov/i) {
    $m1 = 11;
  } elsif($m1 =~ m/cembre/i) {
    $m1 = 12;
  } else {
    $m1 = undef;
  }

  return $m1;
}

sub spider_json_flyer_list {
  my $ua = shift;
  my $url = shift;
  my $request = new HTTP::Request('GET', $url);
  &ua_randomize($ua);
  my $response = $ua->request($request);
  my $content = $response->content;

  my $run_id = undef;
  my $type_id = undef;
  my $id = undef;
  my $merchant_id = undef;
  my $name_id = undef;
  my $merchant_name_id = undef;
  my $valid_from = undef;
  my $valid_to = undef;

  #broken down valid from/to
  my $from_day = undef;
  my $from_month = undef;
  my $from_year = undef;
  my $to_day = undef;
  my $to_month = undef;
  my $to_year = undef;

  #configuration for the available flyers is set inside a script tag as a json
  #object
  my $json = undef;
  if($content =~ m/window\s*\[\s*['"]{1}hostedStack['"]{1}\s*\]\s*=\s*(\[\s*\{.*?\}\s*\]\s*;)/msi) {
    $json = $1;
    printf("detected wishabi flyers configuration on this page\n");
    printf("raw configuration as json object:\n\t%s\n", $json);
  } else {
    printf("no wishabi flyers configuration detected\n");
    return ();
  }

  my @flyers = ();
  my $aflyer = {};
  my $flyer = undef;
  my $first;
  while($json =~ m/\{(.*?)\}/msig) {
    $flyer = $1;
    $aflyer = {};

    if($flyer =~ m/['"]{1}flyer_run_id['"]{1}\s*\:\s*(.*?)[,}]{1}/msi) {
      $run_id = $1;
      $aflyer->{run_id} = $run_id;
    }

    if($flyer =~ m/['"]{1}current_flyer_id['"]{1}\s*\:\s*(.*?)[,}]{1}/msi) {
      $id = $1;
      $aflyer->{id} = $id;
    }

    if($flyer =~ m/['"]{1}flyer_type_id['"]{1}\s*\:\s*(.*?)[,}]{1}/msi) {
      $type_id = $1;
      $aflyer->{type_id} = $type_id;
    }

    if($flyer =~ m/['"]{1}merchant_id['"]{1}\s*\:\s*(.*?)[,}]{1}/msi) {
      $merchant_id  = $1;
      $aflyer->{merchant_id} = $merchant_id;
    }

    if($flyer =~ m/['"]{1}name_identifier['"]{1}\s*\:\s*(.*?)[,}]{1}/msi) {
      $name_id = $1;
      $name_id =~ s/^['"]+//;
      $name_id =~ s/['"]+$//;
      $aflyer->{name_id} = $name_id;
    }

    if($flyer =~ m/['"]{1}merchant_name_identifier['"]{1}\s*\:\s*(.*?)[,}]{1}/msi) {
      $merchant_name_id = $1;
      $merchant_name_id =~ s/^['"]+//;
      $merchant_name_id =~ s/['"]+$//;
      $aflyer->{merchant_name_id} = $merchant_name_id;
    }

    if($flyer =~ m/['"]{1}current_valid_from['"]{1}\s*\:\s*(.*?)[,}]{1}/msi) {
      $valid_from = $1;
      #iso 8601
      if($valid_from =~ m/(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})[-+]{1}(\d{2}):(\d{2})/i) {
        $from_year = $1;
        $from_month = $2;
        $from_day = $3;
        $aflyer->{valid_from} = $from_year . '-' . $from_month . '-' . $from_day . ' ' . '00:00:00';
      }
    }

    if($flyer =~ m/['"]{1}current_valid_to['"]{1}\s*\:\s*(.*?)[,}]{1}/msi) {
      $valid_to = $1;
      if($valid_to =~ m/(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})[-+]{1}(\d{2}):(\d{2})/i) {
        $to_year = $1;
        $to_month = $2;
        $to_day = $3;
        $aflyer->{valid_to} = $to_year . '-' . $to_month . '-' . $to_day . ' ' . '23:59:59';
      }
    }

    #print out scraped fields by key=value regardless of missing fields
    $first = 1;
    foreach my $k (keys %$aflyer) {
      if(!$first) {
        print ', ';
      } else {
        $first = 0;
      }
      print $k . ' = ' . $aflyer->{$k};
    }
    print "\n";

    if((scalar keys %$aflyer) == 8) {
      push @flyers, $aflyer;
    } else {
      printf("this flyer is missing properties: skipping...\n");
      next;
    }
  }

  return @flyers;
}

sub product_discount_exists {
  my $dbh = shift;
  my $external_id = shift;
  my $site = shift;
  my $valid_until = shift;
  my $qh = $dbh->prepare('SELECT COUNT(1) FROM product_discount WHERE external_id=? AND site_sysname=? AND valid_until=?');
  $qh->execute($external_id, $site, $valid_until);
  my $count = undef;
  $qh->bind_columns(undef, \$count);
  $qh->fetch();
  return (($count > 0)? 1: 0);
}

sub keyword_exists {
  my $dbh = shift;
  my $keyword = shift;
  my $qh = $dbh->prepare('SELECT id FROM keyword WHERE keyword=?');
  $qh->execute($keyword);
  my $id = undef;
  $qh->bind_columns(undef, \$id);
  $qh->fetch();
  return $id;
}

sub photo_exists {
  my $dbh = shift;
  my $sha1 = shift;
  my $qh = $dbh->prepare('SELECT id FROM photo WHERE sha1=?');
  $qh->execute($sha1);
  my $id = undef;
  $qh->bind_columns(undef, \$id);
  $qh->fetch();
  return $id;
}

sub insert_photos {
  my $dbh = shift;
  my $ua = shift;
  my @photos = shift;
  my $spidered_images_dir = shift;
  my $product_discount_id = shift;
  my $count = 0;
  foreach my $photo_link (@photos) {
    if($photo_link =~ m/\.([^\.]+)$/) {
      my $img_ext = $1;
      my $file = microtime() . '.' . $img_ext;
      my $res = $ua->mirror($photo_link, $spidered_images_dir . $file);
      if($res->is_success) {
        my $sha1 = digest_file_hex($spidered_images_dir . $file, 'SHA-1');
        my $photo_id = &photo_exists($dbh, $sha1);
        if(!defined($photo_id)) {
          $dbh->do("INSERT INTO photo (id, path, file, sha1, created_at, updated_at) VALUES (NULL, '$spidered_images_dir', '$file', '$sha1', CURRENT_TIMESTAMP, NOW())");
          $photo_id = $dbh->{mysql_insertid};
        }
        $dbh->do("INSERT INTO product_discount_photo VALUES (NULL, $product_discount_id, $photo_id)");
	++$count;
      } else {
        print color('red');
        printf("can't mirror this photo: %s\n", $res->status_line);
        print color('reset');
      }
    } else {
      print color('red');
      printf("can't determine extension\n");
      print color('reset');
    }
  }
  return $count;
}

sub db_reset {
  my $dbh = shift;
  $dbh->do('DELETE FROM product_discount');
  $dbh->do('DELETE FROM product_discount_keyword');
  $dbh->do('DELETE FROM product_discount_photo');
  $dbh->do('DELETE FROM product_discount_group');
  $dbh->do('DELETE FROM `group`');
  $dbh->do('DELETE FROM photo');
  $dbh->do('DELETE FROM keyword');
  $dbh->do('DELETE FROM site');
  $dbh->do('ALTER TABLE product_discount AUTO_INCREMENT = 0');
  $dbh->do('ALTER TABLE product_discount_keyword AUTO_INCREMENT = 0');
  $dbh->do('ALTER TABLE product_discount_photo AUTO_INCREMENT = 0');
  $dbh->do('ALTER TABLE product_discount_group AUTO_INCREMENT = 0');
  $dbh->do('ALTER TABLE `group` AUTO_INCREMENT = 0');
  $dbh->do('ALTER TABLE photo AUTO_INCREMENT = 0');
  $dbh->do('ALTER TABLE keyword AUTO_INCREMENT = 0');
  $dbh->do('ALTER TABLE site AUTO_INCREMENT = 0');
  my @files = </var/www/spidering_env/spidered_images/*.{jpg,jpeg,png,gif,bmp,tif}>;
  unlink @files;
}

sub db_seed {
  my $dbh = shift;
  $dbh->do("INSERT INTO site VALUES (NULL, 'iga', 'IGA', '/var/www/html/logo/iga.png', 'iga.png');");
  $dbh->do("INSERT INTO site VALUES (NULL, 'maxi', 'Maxi', '/var/www/html/logo/maxi.svg', 'maxi.svg');");
  $dbh->do("INSERT INTO site VALUES (NULL, 'superc', 'Super C', '/var/www/html/logo/superc.png', 'superc.png');");
  $dbh->do("INSERT INTO site VALUES (NULL, 'metro', 'Metro', '/var/www/html/logo/metro.png', 'metro.png');");
  $dbh->do("INSERT INTO site VALUES (NULL, 'brunet', 'Brunet', '/var/www/html/logo/brunet.png', 'brunet.png');");
  $dbh->do("INSERT INTO site VALUES (NULL, 'jeancoutu', 'Jean Coutu', '/var/www/html/logo/jeancoutu.png', 'jeancoutu.png');");
  $dbh->do("INSERT INTO site VALUES (NULL, 'uniprix', 'Uniprix', '/var/www/html/logo/uniprix.png', 'uniprix.png');");
  $dbh->do("INSERT INTO site VALUES (NULL, 'familiprix', 'Familiprix', '/var/www/html/logo/familiprix.svg', 'familiprix.svg');");
  $dbh->do("INSERT INTO site VALUES (NULL, 'walmart', 'Walmart', '/var/www/html/logo/walmart.svg', 'walmart.svg');");
  $dbh->do("INSERT INTO site VALUES (NULL, 'provigo', 'Provigo', '/var/www/html/logo/provigo.png', 'provigo.png');");
}

sub microtime {
  my $asFloat = 0;
  if(@_) {
    $asFloat = shift;
  }
  (my $epochseconds, my $microseconds) = gettimeofday;
  my $mt = undef;
  if($asFloat) {
    while(length("$microseconds") < 6) {
      $microseconds = "0$microseconds";
    }
    $mt = "$epochseconds.$microseconds";
  } else {
    #modded to generate unique filename w/o a whitespace
    $mt = "$epochseconds" . '_' . "$microseconds";
  }
  return $mt;
}

sub random_ua {
  my $rua = undef;
  open FILE, "<ua.txt" or die "could not open ua.txt: $!\n";
  rand($.) < 1 and ($rua = $_) while <FILE>;
  close FILE;
  chomp($rua);
  print "random ua: $rua\n";
  return $rua;
}

sub ua_randomize {
  my $ua = shift;
  $ua->agent(&random_ua());
}

1;
