<?php

include('/var/www/html/conf.php');
include('/var/www/html/include/func.inc.php');

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    printf("Error: %s\n<br/>", $e->getMessage());
    die;
}

$dt = get_current_weekly_deals_period();

try {
    $stmt = $conn->prepare("
SELECT group_id,
COUNT(DISTINCT product_discount_id) AS `count`,
GROUP_CONCAT(DISTINCT product_discount_id ORDER BY product_discount_id DESC) AS pdids,
MAX((100 - (pd.discount / pd.price * 100))) AS best_discount_pct
FROM product_discount_group pdg
JOIN product_discount pd ON pd.id=pdg.product_discount_id
WHERE
 valid_from <= '" . $dt[0]->format('Y-m-d H:i:s') . "' and
 valid_until >= '" . $dt[1]->format('Y-m-d H:i:s') . "'
GROUP BY group_id
HAVING COUNT(DISTINCT product_discount_id) > 0
ORDER BY `count` DESC
    ");

    $stmt->execute();
} catch(PDOException $e) {
    printf("Error: %s\n<br/>", $e->getMessage());
    die;
}

while(($grp = $stmt->fetch(PDO::FETCH_OBJ))) {
    try {
        $set = $conn->prepare("UPDATE product_discount_group SET best_discount_pct = :best_discount_pct WHERE group_id = :group_id");
        $set->bindParam(':best_discount_pct', $grp->best_discount_pct, PDO::PARAM_STR);
        $set->bindParam(':group_id', $grp->group_id, PDO::PARAM_INT);
        $set->execute();
    } catch(PDOException $e) {
        printf("Error: %s\n<br/>", $e->getMessage());
        die;
    }
}

?>
