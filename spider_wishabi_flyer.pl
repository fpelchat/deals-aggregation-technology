#!/usr/bin/perl
#Copyright (c) 2012-2019 Frederick Pelchat

use strict;
use warnings;

use LWP::UserAgent;
use HTTP::Cookies;
use HTTP::Request;
use HTTP::Response;
use HTML::Entities; #decode html entities
use Cwd qw(getcwd); #needed for saving photos
use DBI; #generic database access
use File::Glob qw(:globally :nocase); #delete files using a pattern
use Time::HiRes;
use spider;
use IO::Socket::SSL qw(SSL_VERIFY_NONE);

############
### conf ###
############
#XXX dump env
#foreach (sort keys %ENV) {
#  print "$_  =  $ENV{$_}\n";
#}

my $filename = 'cant.log';
open(my $fh, '>:encoding(UTF-8)', $filename) or die "could not open file '$filename' $!";

my $type = 'spider';
my $site = $ENV{'SITE_SYSNAME'};
my $merchant_id = $ENV{'MERCHANT_ID'};
my $local_app_domain = $ENV{'LOCAL_APP_DOMAIN'};
my $external_app_domain = $ENV{'EXTERNAL_APP_DOMAIN'};
my $merchant_dir = $ENV{'MERCHANT_DIR'};
my $default_url_params = $ENV{'DEFAULT_URL_PARAMS'};

my $item_url_fmt = 'https://' . $external_app_domain . '/flyer_item/tabbed/%s?' . $default_url_params;

my $db_user = 'root';
my $db_pass = 'toortoor';
my $db_host = 'cook.cirzl5tv0ibo.us-east-2.rds.amazonaws.com';
my $db_port = '3306';
my $db_name = 'cook';
###########
### end ###
###########

printf("spidering wishabi flyer: %s\n", $site);

#intialize database connection
#required for item's names and descriptions
my $dbh = DBI->connect("DBI:mysql:$db_name;host=$db_host;port=$db_port", $db_user, $db_pass, {RaiseError => 1});
#required for keywords
my $dbh_utf8 = DBI->connect("DBI:mysql:$db_name;host=$db_host;port=$db_port", $db_user, $db_pass, {RaiseError => 1});
$dbh_utf8->do('SET NAMES utf8');

#initialize lwp

#$ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
#$ENV{HTTPS_DEBUG} = 1;
#$NET::HTTPS::SSL_SOCKET_CLASS = 'IO::Socket::SSL';
my $ua = LWP::UserAgent->new(ssl_opts => {verify_hostname => 0, SSL_verify_mode => SSL_VERIFY_NONE});

$ua->cookie_jar(HTTP::Cookies->new(
  'file' => "cookie_jar_$site.lwp",
  'autosave' => 1
));
#end of bootstrap

#XXX uncomment to use proxy
#$ua->proxy(['http', 'ftp', 'https'], 'http://PROXY:8080');

sub spider_item {
  my $item_id = undef;
  my $category = undef;

  if(@_ == 2) {
    $item_id = shift;
    $category = shift;
  } else {
    die "item id and category are mandatory arguments for wishabi flyers spider function\n";
  }

  my $url2spider = sprintf($item_url_fmt, $item_id);

  print "spidering: $url2spider\n";

  my $request = new HTTP::Request('GET', $url2spider);
  &ua_randomize($ua);
  my $response = $ua->request($request);
  my $content = $response->content;

  my $external_id = $item_id;

  my $title = undef;
  if($content =~ m/<h2[^>]+id="item-title"[^>]*>(.*?)<\/h2>/si) {
    $title = $1;
    $title =~ s/^\s+//;
    $title =~ s/\s+$//;
    $title =~ s/\s+/ /;
    $title = decode_entities($title);
  } elsif($content =~ m/<div[^>]+class\s*=\s*['"]{1}\s*name\s*['"]{1}[^>]*>\s*(.+?)\s*<\/div>/si) {
    $title = $1;
    $title =~ s/\s{2,}/ /g;
    $title = decode_entities($title);
  }

  my $discount = undef;
  my $num_of_size = undef;
  my $size = undef;
  my $size_unit = undef;
  my $cost_per = undef;
  my $cost_per_unit = undef;
  if($content =~ m/<div[^>]+class=['"]{0,1}price['"]{0,1}[^>]*>(.*?)<\/div>/si) {
    $discount = $1;
    $discount =~ s/\s*<span[^>]+>\s*/ /g;
    $discount =~ s/\s*<\/span>\s*/ /g;
    $discount =~ s/,/./sg;
    $discount =~ s/^\s+//;
    $discount =~ s/\s+$//;
    $discount =~ s/\s+/ /;
    $discount = decode_entities($discount);

    printf("price before switch/case: %s\n", $discount);

    if($discount =~ m/\s*(\d+)\s*(?:\/|pour|for)\s*(\d+)\s*[\.\,]{1}\s*(\d+)\s*\$/si) {
      print "case 1 : x / price\n";
      my $price = $2 . '.' . $3;
      my $count = $1;
      if($count < 0 || $count > 1000) {
        print "invalid count $count for price $price\n";
        return 456;
      }
      $discount = $price / $count;
    } elsif($discount =~ m/\s*(\d+)\s*[\.\,]{1}\s*(\d+)\s*\$\s+(lb|kg)/si) {
      print "case 2a : price unit\n";
      $discount = $1 . '.' . $2;
      my $unit = $3;
      $num_of_size = 1;
      $size = 1;
      $size_unit = $unit;
      $cost_per = $discount;
      $cost_per_unit = $unit;
    } elsif($discount =~ m/\s*(\d+)\s*[\.\,]{1}\s*(\d+)\s*\$\s*\/\s*(lb|kg)/si) {
      print "case 2b : price / unit\n";
      $discount = $1 . '.' . $2;
      my $unit = $3;
      $num_of_size = 1;
      $size = 1;
      $size_unit = $unit;
      $cost_per = $discount;
      $cost_per_unit = $unit;
    } elsif($discount =~ m/\s*(\d+)\s*\.\s*(\d+)\s*\$\s*ch/si) {
      print "case 3\n";
      $discount = $1 . '.' . $2;
    } elsif($discount =~ m/\s*(\d+)\s*\.\s*(\d+)\s*\$/s) {
      print "case 4\n";
      $discount = $1 . '.' . $2;
    }
  }

  my $desc = undef;
  if($content =~ m/<div[^>]+class\s*=\s*['"]+[^'"]*flyer_item_description[^'"]*['"]+[^>]*>(.*?)<\/div>/si) {
    $desc = $1;
    $desc = decode_entities($desc);
    $desc =~ s/<h\d[^>]*>.*?<\/h\d>//gsi;
    $desc =~ s/<[a-z\d]+[^>]*>//gsi;
    $desc =~ s/<\/[a-z\d]+>//gsi;
    $desc =~ s/\s+/ /gs;
    $desc =~ s/^\s+//gs;
    $desc =~ s/\s+$//gs;
  }

  my $url = undef;
  if($content =~ m/<div[^>]+class=['"]+add_link['"]+>\s*<a[^>]+href=['"]+(.*?)['"]+/si) {
    $url = $1;
  }

  my ($d1, $m1, $y1, $d2, $m2, $y2);
  if($content =~ m/<div[^>]+class='validity'[^>]*>.+?vigueur.+?(\d+)\s+([^\s]+)\s+(\d+).+?(\d+)\s+([^\s]+)\s+(\d+).*?<\/div>/si) {
    $d1 = $1;
    $m1 = $2;
    $y1 = $3;
    $d2 = $4;
    $m2 = $5;
    $y2 = $6;
    $m1 = &str_month_to_num($m1);
    $m2 = &str_month_to_num($m2);
  }

  if(!defined($title) || !defined($discount) || !defined($d1) || !defined($m1) || !defined($y1) || !defined($d2) || !defined($m2) || !defined($y2)) {
    return 333;
  }

  #detect dup asap so that we dont waste cpu time
  if(&product_discount_exists($dbh_utf8, $external_id, $site, sprintf('%04d-%02d-%02d 23:59:59', $y2, $m2, $d2))) {
    printf("product discount already spidered: %s:%s: skipping...\n", $site, $external_id);
    return 666;
  }

  printf("name: %s\ndesc: %s\ndiscount: %s\nurl: %s\nvalidity from: %s\nvalidity to: %s\n",
    defined($title)? $title: 'NULL',
    defined($desc)? $desc: 'NULL',
    defined($discount)? $discount: 'NULL',
    defined($url2spider)? $url2spider: 'NULL',
    sprintf('%04d-%02d-%02d 00:00:00', $y1, $m1, $d1),
    sprintf('%04d-%02d-%02d 23:59:59', $y2, $m2, $d2));

  #extract all photo urls
  my @photos = ($content =~ m/<td>[^<]*<img[^>]+src="(.+?)"[^>]*>/msg);

  #show them
  print join("\n", @photos);
  print "\n";

  my $qt_title = !defined($title)? 'NULL': $dbh_utf8->quote($title);
  my $qt_desc = !defined($desc)? 'NULL': $dbh_utf8->quote($desc);
  my $qt_discount = !defined($discount)? 'NULL': $dbh_utf8->quote($discount);
  my $qt_url = !defined($url2spider)? 'NULL': $dbh_utf8->quote($url2spider);
  my $qt_external_id = !defined($external_id)? 'NULL': $dbh_utf8->quote($external_id);
  my $qt_size = !defined($size)? 'NULL': $dbh_utf8->quote($size);
  my $qt_size_unit = !defined($size_unit)? 'NULL': $dbh_utf8->quote($size_unit);
  my $qt_num_of_size = !defined($num_of_size)? 'NULL': $dbh_utf8->quote($num_of_size);
  my $qt_cost_per = !defined($cost_per)? 'NULL': $dbh_utf8->quote($cost_per);
  my $qt_cost_per_unit = !defined($cost_per_unit)? 'NULL': $dbh_utf8->quote($cost_per_unit);

  my $cwd = getcwd();
  my $spidered_images_dir = 'spidered_images';

  $dbh_utf8->do("
INSERT INTO product_discount (id, site_sysname, brand, title, `desc`, spidered_at, num_of_size, size, size_unit, discount, price, cost_per, cost_per_unit, made_in_quebec, valid_from,
valid_until, external_id, url) VALUES (NULL, '$site', NULL, $qt_title, $qt_desc, CURRENT_TIMESTAMP, $qt_num_of_size, $qt_size, $qt_size_unit, $qt_discount, NULL, $qt_cost_per, $qt_cost_per_unit,
0, '$y1-$m1-$d1 00:00:00', '$y2-$m2-$d2 23:59:59', $qt_external_id, $qt_url)
  ");

  my $product_discount_id = $dbh_utf8->{mysql_insertid};

  my $sql_fmt = "INSERT INTO keyword (id, keyword) VALUES (NULL, %s)";
  my @keywords = ($category);
  my @kwids = ();
  foreach my $kw (@keywords) {
    $kw = decode_entities($kw);
    my $keyword_id = &keyword_exists($dbh_utf8, $kw);
    if(!defined($keyword_id)) {
      my $qt_keyword = $dbh_utf8->quote($kw);
      my $sql = sprintf($sql_fmt, $qt_keyword);
      $dbh_utf8->do($sql);
      $keyword_id = $dbh_utf8->{mysql_insertid};
    }
    push(@kwids, $keyword_id);
  }

  foreach my $kwid (@kwids) {
    $dbh_utf8->do("INSERT INTO product_discount_keyword (id, product_discount_id, keyword_id) VALUES (NULL, $product_discount_id, $kwid)");
  }

  foreach my $photo_link (@photos) {
    my $img_ext = 'jpg';
    my $file = microtime().'.'.$img_ext;
    $ua->mirror($photo_link, './'.$spidered_images_dir.'/'.$file);
    $dbh_utf8->do("INSERT INTO photo (id,path,file,created_at,updated_at) VALUES (NULL, '$cwd/$spidered_images_dir', '$file', CURRENT_TIMESTAMP, NOW())");
    my $photo_id = $dbh_utf8->{mysql_insertid};
    $dbh_utf8->do("INSERT INTO product_discount_photo VALUES (NULL, $product_discount_id, $photo_id)");
  }

  return 0;
}

sub spider_all {
  my $default_flyer_url = 'https://' . $external_app_domain . '/flyers/' . $merchant_dir . '/grid_view/Z?' . $default_url_params;
  printf("attempting to fetch available flyers using url:\n\t%s\n", $default_flyer_url);
  my @flyers = &spider_json_flyer_list($ua, $default_flyer_url);

  NEXT_FLYER: for my $href (@flyers) {
    my $specific_flyer_url = 'https://' . $external_app_domain . '/flyers/' . $href->{name_id} . '/grid_view/' . $href->{run_id} . '?' . $default_url_params;

    printf("spidering flyer:\n\t%s\n", $specific_flyer_url);

    my $request = new HTTP::Request('GET', $specific_flyer_url);
    &ua_randomize($ua);
    my $response = $ua->request($request);
    my $content = $response->content;

    my @categories = ($content =~ m/<button[^>]+class\s*=\s*['"]{1}[^'"]*category-link[^'"]*['"]{1}[^>]+data-value\s*=\s*['"]{1}\s*([^'"]+)\s*['"]{1}[^>]*>\s*(.*?)\s*</gsi);

    NEXT_CATEGORY: while(my $cat = shift(@categories)) {
      my $label = shift(@categories);
      printf("parsing category: %s label: %s\n", $cat, $label);
      if($cat =~ m/autre/i) {
        #printf("skipping others category because its mostly links to external stuff like google app, etc...\n");
        #next NEXT_CATEGORY;
      } elsif($cat =~ m/(?:toute|tous)/i) {
        printf("skipping all category because the only explicit keyword we have for wishabi flyers is the category itself\n");
        next NEXT_CATEGORY;
      }

      my $category_content = undef;

      if($content =~ m/<div[^>]+class\s*=\s*['"]{1}[^'"]*category-$cat[^'"]*['"]{1}[^>]*>.*?<ul[^>]*>(.*?)<\/ul>/msi) {
        $category_content = $1;
      } else {
        printf("no data for this category: skipping...\n", $cat);
        next NEXT_CATEGORY;
      }

      my @items = ($category_content =~ m/<li[^>]+class\s*=\s*['"]{1}[^'"]*item[^'"]*['"]{1}[^>]*>.+?gridViewItemPop\s*\(\s*(\d+)\s*\).+?<\/li>/sig);

      print "current items:\n";
      print join(',', @items);
      print "\n";

      my $count = 0;
      my $num_items_on_this_page = scalar @items;
      my $already_spidered_on_this_page = 0;
      foreach my $i (@items) {
        my $ret = &spider_item($i, $label);
        if($ret == 666) {
          ++$already_spidered_on_this_page;
        } elsif($ret == 333) {
          my $sleepms = 1000;
          my $nattempt = 1;
          my $stoptrying = 0;
          do {
            Time::HiRes::usleep($sleepms);
            $ret = &spider_item($i, $label);
            if($ret == 0) {
              $stoptrying = 1;
            } else {
              $sleepms *= 2;
            }
          } while($nattempt++ < 3 && !$stoptrying);
          if(!$stoptrying) {
            print $fh "manually inspect: $i\n";
          }
        }

        ++$count;
      }
    }
  }
}

#################################################################
#XXX fetch available flyers for specified site
#################################################################
#my @flyers = &spider_json_flyer_list($ua, $items_url);

#for my $href (@flyers) {
#  for my $prop (keys %$href) {
#    print "$prop = " . $href->{$prop} . "\n";
#  }
#  print "\n";
#}

#die;
#################################################################

my ($single) = @ARGV;
if(defined($single)) {
  &spider_item($single);
} else {
  &spider_all();
}
