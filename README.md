# Deals Aggregation Technology

This is the aggregation technology that has been developed over the past 10 years that is used to retrieve weekly deals for many portals over the Internet.

Here's a quick summary of each file:

- spider_wishabi_flyer.pl : spider a wishabi flyer and receive parameters through environment variables
- wishabi.conf.php : wishabi flyers list that we support also very easy to add new ones
- spider_wishabi_flyers.php : read configuration file and feed parameters to the wishabi flyer for each store
- spider_iga.pl : specific spider for IGA store so that we have more informations on discounted products
- spider_maxi.pl : specific spider for Maxi store so that we have more informations on discounted products
- spider.pm : perl library shared across spiders
- ua.txt : anonymize our HTTP requests a little bit based on the most popular User-Agent at the moment
- group.php : group similar products its very useful for the specific type of spider
because they list all kind of products as separate products compared to a flyer retrieved
by the wishabi spider
- set_best_discount_pct.php : order the list of products for each group by best
discounted percentage so that the best deal shows first most of the time its only possible to do that
for products spidered using a specific type of spider because in this case they also provide the original
price (non-discounted)
