#!/bin/bash
DATE=$(date +"%Y-%m-%d_%H:%M:%S_%Z")
cd /usr/local/share/spidering_env
export PERL5LIB=.
./spider_iga.pl >> ./iga-$$-$DATE 2>&1
