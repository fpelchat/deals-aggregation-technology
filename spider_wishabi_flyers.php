#!/usr/bin/env /usr/bin/php

<?php

/************
 *** conf ***
 ************/

$default_url_params = [
  'postal_code' => 'H2Y1T2',
  'locale' => 'fr',
  'is_store_selection' => 'true',
  'type' => 2, //so that we see the header with the other available flyers
  'auto_flyer' => '',
  'sort_by' => '',
];

include('wishabi.conf.php');

/***********
 *** end ***
 ***********/

$spider_washibi_bin = './spider_wishabi_flyer.pl';

$specific_product = $specific_shop = null;

if($argc >= 2) {
  $specific_shop = $argv[1];
}

if($argc >= 3) {
  $specific_product = $argv[2];
}

foreach($shop as $sk => $sv) {
  if($specific_shop !== null && $specific_shop != $sk) { //if specific shop specified...
    continue; //skip the rest
  }

  putenv('SITE_SYSNAME=' . $sk); //the site also called the shop is injected into the spider through an environment variable
  putenv('DEFAULT_URL_PARAMS=' . http_build_query($default_url_params));

  foreach($sv as $k => $v) {
    putenv(strtoupper($k) . '=' . $v);
  }

  system($spider_washibi_bin . ($specific_product !== null? " $specific_product": ''));
}

?>
