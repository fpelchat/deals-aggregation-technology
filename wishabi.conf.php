<?php

$shop = [
  'metro' => [
    'store_code' => 176,
    'merchant_id' => 2269,
    'external_app_domain' => 'ecirculaire.metro.ca',
    'merchant_dir' => 'metro-circulaire',
  ],

  'superc' => [
    'store_code' => 5993,
    'merchant_id' => 2585,
    'external_app_domain' => 'ecirculaire.superc.ca',
    'merchant_dir' => 'superc-circulaire',
  ],

  'brunet' => [
    'store_code' => 15443,
    'merchant_id' => 3753,
    'external_app_domain' => 'ecirculaire.brunet.ca',
    'merchant_dir' => 'brunet-circulaire',
  ],

  'jeancoutu' => [
    'store_code' => 90,
    'merchant_id' => 2521,
    'external_app_domain' => 'circulaireapp.jeancoutu.com',
    'merchant_dir' => 'jeancoutu',
  ],

  'uniprix' => [
    'store_code' => 299479,
    'merchant_id' => 3467,
    'external_app_domain' => 'flyers.uniprix.com',
    'merchant_dir' => 'uniprixpharmacy',
  ],

  'walmart' => [
    'store_code' => 1170,
    'merchant_id' => 234,
    'external_app_domain' => 'flyers.walmart.ca',
    'merchant_dir' => 'walmartcanada-groceryflyer',
  ],

  'provigo' => [
    'store_code' => 8659,
    'merchant_id' => 2338,
    'external_app_domain' => 'flyers.provigo.ca',
    'merchant_dir' => 'provigo',
  ],

  'familiprix' => [
    'store_code' => 'S7727',
    'merchant_id' => 1976,
    'external_app_domain' => 'circulaire.familiprix.com',
    'merchant_dir' => 'familiprix',
  ],

  'pharmaprix' => [
    'store_code' => 1847,
    'merchant_id' => 2170,
    'external_app_domain' => 'flyers.pharmaprix.ca',
    'merchant_dir' => 'pharmaprix',
  ],
];

?>
