#!/usr/bin/perl
#Copyright (c) 2012-2019 Frederick Pelchat

use strict;
use warnings;

use LWP::UserAgent;
use HTTP::Cookies;
use HTTP::Request;
use HTTP::Response;
use HTML::Entities; #decode html entities
use Cwd qw(getcwd); #needed for saving photos
use DBI; #generic database access
use File::Glob qw(:globally :nocase); #delete files using a pattern
use spider;
use POSIX;
use Digest::file qw(digest_file_hex);
use Term::ANSIColor;

#global conf
my $type = 'spider';
my $site = 'maxi';
my $base = 'https://www.maxi.ca';
my $db_user = 'root';
my $db_pass = 'toortoor';
my $db_host = 'cook.cirzl5tv0ibo.us-east-2.rds.amazonaws.com';
my $db_port = '3306';
my $db_name = 'cook';
#end of global conf

print "spidering site: $site\n";

#intialize database connection
my $dbh = DBI->connect("DBI:mysql:$db_name;host=$db_host;port=$db_port", $db_user, $db_pass, {RaiseError => 1});
$dbh->do('set names utf8');

#initialize lwp
my $ua = new LWP::UserAgent;
$ua->cookie_jar(HTTP::Cookies->new(
  'file' => "cookie_jar_$site.lwp",
  'autosave' => 1
));
#end of bootstrap

#XXX uncomment to use proxy
#$ua->proxy(['http', 'ftp', 'https'], 'http://PROXY:8080');

sub spider_item {
  my $product_id = undef;

  my $argc = scalar @_;

  if($argc == 1) {
    $product_id = shift;
  } else {
    die("usage: &spider_item(\$product_id);\n");
  }

  my $url = 'https://www.maxi.ca/api/product/' . $product_id;

  print color('bold yellow');
  print "spidering: $url\n";
  print color('reset');

  #send the http request
  my $hdr = HTTP::Headers->new;
  $hdr->header('Site-Banner' => 'maxi'); #mandatory custom header else the api call will fail

  my $request = new HTTP::Request('GET', $url, $hdr);
  &ua_randomize($ua);
  my $html = $ua->request($request);
  my $content = $html->content;

  #print $content;

  #detect if we already spidered this product as soon as possible so that we
  #dont waste cpu cycles
  my ($d1, $m1, $y1, $d2, $m2, $y2);
  #XXX assume weekly deal all the time?
  if($content =~ m/['"]{1}expiryDate['"]{1}\s*:\s*['"]{1}(\d{4})-(\d{2})-(\d{2})/si) {
    $d1 = undef;
    $m1 = undef;
    $y1 = undef;
    $d2 = $3;
    $m2 = $2;
    $y2 = $1;
    #XXX there is a bug with maxi they say the deal will expire on wednesday @ midnight which means the
    #deal would end on tuesday night it should be wednesday at 23:59:59 or
    #thursday at midnight anyway
    my $time2 = POSIX::mktime(59, 59, 23, $d2, $m2, $y2, 0, 0, 1);
    my $time1 = $time2 - (8 * 24 * 60 * 60) + 1;
    my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime($time1);
    $d1 = $mday;
    $m1 = $mon;
    $y1 = $year;
  } else {
    print color('red');
    print("can't detect expiry date\n");
    print color('reset');
    return 2345;
  }

  if(&product_discount_exists($dbh, $product_id, $site, sprintf('%04d-%02d-%02d 23:59:59', $y2, $m2, $d2))) {
    print color('red');
    printf("already spidered this discounted product which expires @ %s\n", sprintf('%04d-%02d-%02d 23:59:59', $y2, $m2, $d2));
    print color('reset');
    return 666;
  }

  my @photos = ($1) if $content =~ m/['"]{1}mediumRetinaUrl['"]{1}\s*:\s*['"]{1}(.*?)['"]{1}/gsi;
  my $brand = $1 if $content =~ m/['"]{1}brand['"]{1}\s*:\s*['"]{1}(.*?)['"]{1}/si;
  my $title = $1 if $content =~ m/['"]{1}name['"]{1}\s*:\s*['"]{1}(.*?)['"]{1}/si;
  my $sub_content = $1 if $content =~ m/['"]{1}price['"]{1}\s*:\s*{(.*?)}/si or return 741;
  my $discount = $1 if $sub_content =~ m/['"]{1}value['"]{1}\s*:\s*(.*?)[},]{1}/si;
  $sub_content = $1 if $content =~ m/['"]{1}wasPrice['"]{1}\s*:\s*{(.*?)}/si or return 742;
  my $price = $1 if $sub_content =~ m/['"]{1}value['"]{1}\s*:\s*(.*?)[},]{1}/si;
  my $size = $1 if $content =~ m/['"]{1}packageSize['"]{1}\s*:\s*(.*?)[},]{1}/si;
  &trim_json_value($size);
  my $size_unit = $size;
  $size_unit =~ s/^.*?([a-z]+).*?$/$1/si;
  if($size_unit =~ m/^$/) {
    print color('red');
    printf("unsupported size: %s\n", $size);
    print color('reset');
  } else {
    $size_unit = lc $size_unit;
  }
  $size =~ s/^.*?([\d,.]+).*?$/$1/s;
  my $cost_per = undef;
  my $cost_per_unit = undef;
  my $cost_per_quantity = undef;
  $sub_content = $1 if $content =~ m/['"]{1}comparisonPrices['"]{1}\s*:\s*\[(.*?)\]/si;
  while($sub_content =~ m/\{(.*?\})/gsi) {
    my $cost_per_content = $1;
    print "cost per content dump: $cost_per_content\n";
    $cost_per = $1 if $cost_per_content =~ m/['"]{1}value['"]{1}\s*:\s*(.*?)[},]{1}/si;
    $cost_per_unit = $1 if $cost_per_content =~ m/['"]{1}unit['"]{1}\s*:\s*(.*?)[},]{1}/si;
    $cost_per_quantity = $1 if $cost_per_content =~ m/['"]{1}quantity['"]{1}\s*:\s*(.*?)[},]{1}/si;
    if(defined($cost_per) && defined($cost_per_unit) && defined($cost_per_quantity) && $cost_per_unit =~ m/[a-z]+/si) {
      &trim_json_value($cost_per);
      &trim_json_value($cost_per_unit);
      $cost_per_unit = lc $cost_per_unit;
      &trim_json_value($cost_per_quantity);
      print color('green');
      print "found revelant cost per infos: $cost_per_quantity x $cost_per_unit = $cost_per\n";
      print color('reset');
      last;
    } else {
      $cost_per = $cost_per_quantity = $cost_per_unit = undef;
    }
  }

  my $sql_fmt = "INSERT INTO keyword (id, keyword) VALUES (NULL, %s)";
  my @kwids = ();
  $sub_content = $1 if $content =~ m/['"]{1}breadcrumbs['"]{1}\s*:\s*\[(.*?)\]/si;
  while($sub_content =~ m/\{(.*?\})/gsi) {
    my $keyword_content = $1;
    my $kw = $1 if $keyword_content =~ m/['"]{1}name['"]{1}\s*:\s*(.*?)[},]{1}/si;
    if(defined($kw)) {
      &trim_json_value($kw);
      my $keyword_id = &keyword_exists($dbh, $kw);
      if(!defined($keyword_id)) {
        my $qt_keyword = $dbh->quote($kw);
        my $sql = sprintf($sql_fmt, $qt_keyword);
        $dbh->do($sql);
        $keyword_id = $dbh->{mysql_insertid};
      }
      my %valaskey = map {$_ => 1} @kwids;
      if(!exists($valaskey{$keyword_id})) { #XXX sometimes maxi has the same keywords entered twice or more
        push(@kwids, $keyword_id);
        print color('green');
        print "found keyword: $kw\n";
        print color('reset');
      }
    }
  }

  printf("%s id: %s\n", $site, defined($product_id)? $product_id: 'NULL');
  print join("\n", @photos);
  print "\n";
  printf("brand: %s\n", defined($brand)? $brand: 'NULL');
  printf("title: %s\n", defined($title)? $title: 'NULL');
  printf("discount: %s\n", defined($discount)? $discount: 'NULL');
  printf("price: %s\n", defined($price)? $price: 'NULL');
  printf("size: %s\n", defined($size)? $size: 'NULL');
  printf("size unit: %s\n", defined($size_unit)? $size_unit: 'NULL');
  printf("cost per quantity: %s\n", defined($cost_per_quantity)? $cost_per_quantity: 'NULL');
  printf("cost per: %s\n", defined($cost_per)? $cost_per: 'NULL');
  printf("cost per unit: %s\n", defined($cost_per_unit)? $cost_per_unit: 'NULL');
  printf("%04d-%02d-%02d 00:00:00 until %04d-%02d-%02d 23:59:59 inclusively\n", $y1, $m1, $d1, $y2, $m2, $d2);

  my $qt_brand = $dbh->quote($brand);
  my $qt_title = $dbh->quote($title);
  my $qt_discount = $dbh->quote($discount);
  my $qt_price = $dbh->quote($price);
  my $qt_cost_per = defined($cost_per)? $dbh->quote($cost_per): 'NULL';
  my $qt_cost_per_unit = defined($cost_per_unit)? $dbh->quote($cost_per_unit): 'NULL';
  my $qt_cost_per_quantity = defined($cost_per_quantity)? $dbh->quote($cost_per_quantity): 'NULL';
  my $qt_external_id = $dbh->quote($product_id);
  my $qt_url = $dbh->quote($url);
  my $qt_size = $dbh->quote($size);
  my $qt_size_unit = $dbh->quote($size_unit);

  my $cwd = getcwd();
  my $spidered_images_dir = '/var/www/spidering_env/spidered_images/';

  $dbh->do("
INSERT INTO product_discount (id, site_sysname, brand, title, spidered_at, size, size_unit, discount, price, cost_per_quantity, cost_per_unit, cost_per, made_in_quebec, valid_from, valid_until, external_id, url) 
VALUES (NULL, '$site', $qt_brand, $qt_title, CURRENT_TIMESTAMP, $qt_size, $qt_size_unit, $qt_discount, $qt_price, $qt_cost_per_quantity, $qt_cost_per_unit, $qt_cost_per, 0, '$y1-$m1-$d1 00:00:00', 
'$y2-$m2-$d2 23:59:59', $qt_external_id, $qt_url)
  ");

  my $product_discount_id = $dbh->{mysql_insertid};

  foreach my $kwid (@kwids) {
    $dbh->do("INSERT INTO product_discount_keyword (id, product_discount_id, keyword_id) VALUES (NULL, $product_discount_id, $kwid)");
  }

  foreach my $photo_link (@photos) {
    if($photo_link =~ m/\.([^\.]+)$/) {
      my $img_ext = $1;
      my $file = microtime() . '.' . $img_ext;
      my $res = $ua->mirror($photo_link, $spidered_images_dir . $file);
      if($res->is_success) {
        my $sha1 = digest_file_hex($spidered_images_dir . $file, 'SHA-1');
        my $photo_id = &photo_exists($dbh, $sha1);
        if(!defined($photo_id)) {
          $dbh->do("INSERT INTO photo (id, path, file, sha1, created_at, updated_at) VALUES (NULL, '$spidered_images_dir', '$file', '$sha1', CURRENT_TIMESTAMP, NOW())");
          $photo_id = $dbh->{mysql_insertid};
        }
        $dbh->do("INSERT INTO product_discount_photo VALUES (NULL, $product_discount_id, $photo_id)");
      } else {
        print color('red');
        printf("can't mirror this photo: %s\n", $res->status_line);
        print color('reset');
      }
    } else {
      print color('red');
      printf("can't determine extension\n");
      print color('reset');
    }
  }

  return 0;
}

sub spider_all {
  my $root = 'https://www.maxi.ca/Alimentation/Boulangerie/c/MAX001003000000';
  my $api_fmt = 'https://www.maxi.ca/plp/%s?loadMore=true&sort=&filters=&itemsLoadedonPage=%d&_=%d371';
  my $request = new HTTP::Request('GET', $root);
  &ua_randomize($ua);
  my $html = $ua->request($request);
  my $content = $html->content;
  while($content =~ m/<li[^>]+data-level="(.+?)"[^>]*>.*?<a[^>]+href="(.+?)"/gs) { #iterate over each category
  #our duplicate handling code is sitting around the smallest element type which is a discounted product
    my $level = $1; #depth of the category in the category tree 1 being root nodes
    my $url = $2; #full url to parse the category
    if($url =~ m/\/(MAX.+?)$/) {
      my $category = $1;
      my $total = 0; #total products parsed so far for this category
      my %fetched_products = (); #used to track duplicate products for this category
      my $already_fetched_items_on_this_page = 0;
      my $num_items_on_this_page = 0;
      do {
        my $api_call = sprintf($api_fmt, $category, $total, time());
        my $req2 = new HTTP::Request('GET', $api_call);
        printf("calling: %s\n", $api_call);
        &ua_randomize($ua);
        my $html2 = $ua->request($req2);
        my $content2 = $html2->content;
        my @items = ($content2 =~ m/(<div class="item">.+?<\/form>.+?<\/div>.+?<\/div>)/gs);
        $num_items_on_this_page = scalar @items;
        printf("number of items on this page: %d\n", $num_items_on_this_page);
        if($num_items_on_this_page == 0) {
          last;
        }
        $total += $num_items_on_this_page;
        $already_fetched_items_on_this_page = 0;
        foreach my $item (@items) {
          my $product_id = undef;
          if($item =~ m/data-product-id="(.+?)"/) {
            $product_id = $1;
            if(exists $fetched_products{$product_id} && $fetched_products{$product_id}) {
              ++$already_fetched_items_on_this_page;
            } else {
              $fetched_products{$product_id} = 1;
            }
          } else {
            ++$already_fetched_items_on_this_page;
            printf("can't detect product id: %s\n", $item);
            next;
          }
          if($item =~ m/<span[^>]+class\s*=\s*['"]{1}[^'"]*deal-expiry[^'"]*['"]{1}[^>]*>/si) {
            if(&spider_item($product_id) == 666) {
              ;
            }
          }
        }
        printf("already fetched items on this page: %d\n", $already_fetched_items_on_this_page);
      } while($already_fetched_items_on_this_page != $num_items_on_this_page); #loop to fetch all products for a specific category
    }
  }
}

my ($single) = @ARGV;
if(defined($single)) {
  &spider_item($single);
} else {
  &spider_all();
}
